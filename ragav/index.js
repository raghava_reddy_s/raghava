
const mongoose = require('./app/modal/db.conn.js');
const bodyParser = require('body-parser');
const CONFIG = require('./app/config/config.js');
const express = require('express');
const users=require('./routes/user.js')
let app = express();
app.set('port', CONFIG.PORT);

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(bodyParser.raw());

app.use(users)

app.listen(app.get('port'), function(req, res) {
 console.log("Server running....");
 console.log("magic happens on port number" + app.get('port'));
});