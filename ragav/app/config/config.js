
<!--
      Mongo DB connectivity information stored in a configuration file
-->
const port =4090; // mongo DB port number
const host = '127.0.0.1'; // host name where mongo DB is residing
const dburl = 'mongodb://ds121373.mlab.com:21373/tradewin'; // Mongo DB tradewin data base connectivity string
const user = 'tradewin'; // user to login
const pass = 'tradewin1'; // password to login to database
const authSource = 'tradewin'; // ?
const secret = 'supersecret';
// to make this module exportable (to be used else where in the project)
module.exports={
  PORT : port,
  HOST : host,
  DBURL : dburl,
  USER:user,
  PASS:pass,
  AUTH:authSource,
  AUTHKEY:secret
   };